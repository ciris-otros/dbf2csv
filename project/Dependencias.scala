import sbt._
import Keys._

/**
* Author: Piousp
* Company: Ciris Informatic Solutions
*/
object Dependencias{

  private object Librerias {

    private lazy val akkaVersion = "2.3.4"
    
    lazy val scalaTest = test("org.scalatest" %% "scalatest" % "2.2.0")
    lazy val jodatime = compile("joda-time" % "joda-time" % "2.3")
    lazy val javadbf = compile("com.linuxense" % "javadbf" % "0.4.0")

    private def compile   (deps: ModuleID*): Seq[ModuleID] = deps map (_ % "compile")
    private def provided  (deps: ModuleID*): Seq[ModuleID] = deps map (_ % "provided")
    private def test      (deps: ModuleID*): Seq[ModuleID] = deps map (_ % "test")
    private def runtime   (deps: ModuleID*): Seq[ModuleID] = deps map (_ % "runtime")
    private def container (deps: ModuleID*): Seq[ModuleID] = deps map (_ % "container")
  }	//object
  
//##############################################################################
//  Librerías
  import Librerias._
  lazy val base = scalaTest ++ jodatime ++ javadbf
} //Dependencias

object Resolvers{

	val basic = Seq(
		Resolver.mavenLocal,
    Resolver.typesafeRepo("releases"),
    Resolver.sonatypeRepo("releases"),
    Resolver.typesafeRepo("snapshots"),
    Resolver.sonatypeRepo("snapshots")
	)
}	//object
