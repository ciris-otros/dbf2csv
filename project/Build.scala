import sbt._
import Keys._
import sbtassembly.Plugin._
import AssemblyKeys._

/**
  Author: Piousp
  Company: Ciris Informatic Solutions
*/
object MedicirisBuild extends Build {

	override val settings: Seq[sbt.Def.Setting[_]] = (super.settings ++ Ajustes.deConstruccion).toSeq

  // -------------------------------------------------------------------------------------------------------------------
  // Root Project
  // -------------------------------------------------------------------------------------------------------------------
  lazy val root = Project(id = "dbf2csv", base = file("."), settings = ajustesBasicos("1.0.0", assemblySettings, Nil))
    .settings(libraryDependencies ++= Dependencias.base)
    .settings(outputPath in assembly := new File("./dbf2csv.jar"))

  private def ajustesBasicos(versionModulo: String,
                             extraAjustes: Seq[sbt.Def.Setting[_]],
                             extraLibs:  Traversable[ModuleID]): Seq[sbt.Def.Setting[_]] = {
    Ajustes.basicos ++ extraAjustes ++
      Seq(version := versionModulo, libraryDependencies ++= Dependencias.base ++ extraLibs)
  } //def
}	//object
